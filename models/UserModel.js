var mongoose = require("mongoose");
const relationShip = {
	maried: 'maried',
	single: 'single',
}
var UserSchema = new mongoose.Schema({
	firstName: { type: String, required: true },
	lastName: { type: String, required: true },
	email: { type: String, required: true },
	password: { type: String, required: true },
	telephone: { type: Number, required: true },
	relationShipStatus: {
		type: String, enum: [
			relationShip.maried,
			relationShip.single
		], required: true
	},
	birthDate: { type: Date, required: true },
	gender: { type: String, required: true },
	interests: [{ type: String, required: false }], //stand For hashtags
	jobRole: { type: String, required: false },
	favoriteEvents: [{ type: mongoose.Schema.ObjectId, ref: "Event", required: false }],
	position: {
		langitude: { type: Number, required: true, default: 0 },
		latitude: { type: Number, required: true, default: 0 },
	},
	bookedEvents: [{ type: mongoose.Schema.ObjectId, ref: "Event", required: false }],
	likedPosts: [{ type: mongoose.Schema.ObjectId, ref: "News", required: false }],
	AccountStatus: { type: String, required: true },
	customerIdInnvoice: { type: String, required: true }
}, { timestamps: true });

// Virtual for user's full name
UserSchema
	.virtual("fullName")
	.get(function () {
		return this.firstName + " " + this.lastName;
	});

module.exports = mongoose.model("User", UserSchema, "User");