var mongoose = require("mongoose");

var EventSchema = new mongoose.Schema({
    title: { type: String, required: true },
    description: { type: String, required: true },
    startDate: { type: Date, required: true },
    endDate: { type: Date, required: true },
    position: {
        langitude: { type: Number, required: true },
        latitude: { type: Number, required: true }
    },
    interests: { type: String, required: true }, //stand For hashtag
    image: { type: String, required: true },
    video: { type: String, required: true },
    price: { type: Number, required: true },
    capacity: { type: Number, required: true },
    creator: { type: mongoose.Schema.ObjectId, ref: "Organiser", required: false },
    attendees: [{ type: mongoose.Schema.ObjectId, ref: "User", required: false }],
    completed: { type: Boolean, required: false },
    placesFull: { type: Boolean, required: false }

}, { timestamps: true });


module.exports = mongoose.model("Event", EventSchema, "Event");