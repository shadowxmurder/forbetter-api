var mongoose = require("mongoose");
const newsType = {
    photo: 'photo',
    video: 'video',
    survey: 'survey',
    gif: 'gif'
}
var NewsSchema = new mongoose.Schema({
    type: {
        type: String, enum: [
            newsType.photo,
            newsType.video,
            newsType.survey,
            newsType.gif
        ], required: true
    },
    description: { type: String, required: true },
    images: [{ type: String, required: true }],
    comments: [{
        user: { type: mongoose.Schema.ObjectId, ref: "User", required: false },
        text: { type: String, required: false },
        image: { type: String, required: false },
        hashtag: { type: String, required: false }
    }],
    surveys: [{
        name: { type: String, required: false },
        votes: { type: Number, required: false }
    }],
    numberOfLikes: { type: Number, required: false, default: 0 },
    tags: [{ type: mongoose.Schema.ObjectId, ref: "User", required: false }],
    creator: { type: mongoose.Schema.ObjectId, ref: "User", required: false }

}, { timestamps: true });


module.exports = mongoose.model("News", NewsSchema, "News");