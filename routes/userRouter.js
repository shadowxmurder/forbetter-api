var express = require('express');
const { signup, updateInfo, usersList } = require('../controllers/userController')
const { askChatBot } = require('../controllers/chatBotController')
const catchMiddleware = require('../middlewares/api')
var router = express.Router();
const { authorize, AUTH_ROLES } = require('../middlewares/auth')
const { USER } = AUTH_ROLES
router.post("/signup", catchMiddleware(signup))
router.put("/updateInfo", authorize(USER), catchMiddleware(updateInfo))
router.get("/", catchMiddleware(usersList))
router.post("/askChatBot", catchMiddleware(askChatBot))
module.exports = router;
