var express = require('express');
const { loginUser, loginWithFacebook, loginWithGoogle, loginWithLinkedIn } = require('../controllers/authorityController')
const catchMiddleware = require('../middlewares/api')
var router = express.Router();
router.post("/login", catchMiddleware(loginUser))
router.post("/loginWithFacebook", catchMiddleware(loginWithFacebook))
router.post("/loginWithGoogle", catchMiddleware(loginWithGoogle))
router.post("/loginWithLinkedIn", catchMiddleware(loginWithLinkedIn))
module.exports = router;
