const express = require("express");
const authRouter = require("./authRouter");
const userRouter = require("./userRouter");
var router = express.Router();
router.get('/', (req, res) => {
  res.status(200).json("Backend server working properly! 🙌 ");
})

router.use("/auth/", authRouter);
router.use("/user/", userRouter);

module.exports = router;