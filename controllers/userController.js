const UserService = require("../services/UserService")
const { success, error } = require("../middlewares/response");
const signup = (req, res, next) => {
    new UserService().signup(req.body)
        .then((info) => res.status(200).json(success("SIGNUP_USER", info, res.statusCode)))
        .catch(err => res.status(err.code || 500).json(error(err.status, err.code) || "internal error"))
}
const updateInfo = (req, res, next) => {
    new UserService().updateInformation(req.body, req.user)
        .then((info) => res.status(200).json(success("UPDATE_INFO", info, res.statusCode)))
        .catch(err => res.status(err.code || 500).json(error(err.status, err.code) || "internal error"))
}
const usersList = (req, res, next) => {
    new UserService().usersList()
        .then((info) => res.status(200).json(success("USERS_LIST", info, res.statusCode)))
        .catch(err => res.status(err.code || 500).json(error(err.status, err.code) || "internal error"))
}
module.exports = {
    signup,
    updateInfo,
    usersList
}