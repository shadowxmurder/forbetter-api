const ChatBotServices = require("../services/ChatBotServices")
const {success, error} = require("../middlewares/response");
const askChatBot = (req, res, next) => {
  new ChatBotServices().askChatBot(req.body)
  .then((info) => res.status(200).json(success("CHAT_BOT", info, res.statusCode)))
  .catch(err => res.status(err.code||500).json(error(err.status, err.code)||"internal error"))
}
module.exports = {
    askChatBot
}