const { NlpManager } = require("node-nlp");

class ChatBotServices {
  constructor() { }

  async initialiseChatBot(user) {
    try {
    const message = `good morning ${user.firstName} ${user.lastName} ! welcome to the for better application! how may i help you?`
    return message
    } catch (error) {
      throw error
    }
  }
  async askChatBot({message, language = "en"}) {
    try {
    const manager = new NlpManager({ languages: [language] });
    manager.load();
    const response = await manager.process(language, message);
    return {response: response.answer}
    } catch (error) {
      throw error
    }
  }
}
module.exports = ChatBotServices
