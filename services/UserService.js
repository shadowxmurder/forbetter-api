const UserModel = require("../models/UserModel")
const bcrypt = require('bcryptjs');
const { distance } = require('../helpers/distance')
const { AuthorizationError, NotFoundError, LimitEssay, AlreadyExistError, InternalError, BadRequestError } = require('../errors/appError');
const { sign } = require("jsonwebtoken")
const StripeService = require("../services/StripeService")
const { AUTH_ROLES } = require('../middlewares/auth')
const nodemailer = require("nodemailer");
class UserService {
  constructor() { }
  async signup(body) {
    try {
      const user = await UserModel.findOne({ email: body.email }).lean() ||
        await UserModel.findOne({ telephone: body.telephone }).lean()
      if (user) throw new AlreadyExistError()
      const { password } = body
      let registredUser = new UserModel(body)
      registredUser.password = bcrypt.hashSync(password, 12);
      registredUser.passwordSalt = bcrypt.hashSync(password, 12);
      let customerIdInnvoice = await StripeService.createCustomerId(registredUser)
      registredUser.customerIdInnvoice = customerIdInnvoice.id
      await registredUser.save()
      return registredUser
    } catch (error) {
      console.log(error)
      throw error
    }
  }
  static async generateAuthToken(user) {
    const token = sign({ _id: user._id, role: AUTH_ROLES.USER }, process.env.TOKEN_PASSWORD, {
      expiresIn: "7d",
    });
    return token
  }
  async changePassword(body, user) {
    const { password, newPassword } = body;

    try {
      let checkIfPasswordMatch = await bcrypt.compare(password, user.password);
      if (!checkIfPasswordMatch) throw new AuthorizationError();
      const updateQuery = {
        $set: {
          password: bcrypt.hashSync(newPassword, 12),
          passwordSalt: bcrypt.hashSync(newPassword, 12),
        },
      };
      let updated = await UserModel.findOneAndUpdate(
        { _id: user._id },
        updateQuery, {
        projection: { "password": 0, "passwordSalt": 0, "creditCardInfos.CCV": 0 }
      }
      );

      //new TwilioService().sendSms("test message", user.telephone);
      return updated;
    } catch (error) {
      throw error
    }
  }
  async updateInformation(body, user) {
    try {
      await UserModel.findOneAndUpdate(
        { _id: user._id },
        {
          $set: body,
        },
        { returnOriginal: false }
      ).lean();
      const newUser = await UserModel.findById(user._id, { "password": 0 });
      return newUser
    } catch (error) {
      throw error
    }
  }
  async forgetPassword(body) {
    const { code, email } = body
    try {
      const user = await UserModel.findOne({ email }).lean();
      let html = `<a href=${process.env.APP_REACT}/ForgetPassword?email=${email}> <h2>reset your password here </h2></a><br/> Best<br/> My dish support Team`;
      if (code === user.confirmOTP) {
        let transporter = nodemailer.createTransport({
          host: "smtp.gmail.com",
          port: 587,
          secure: false, // true for 465, false for other ports
          auth: {
            user: process.env.MAIL_GMAIL, // generated ethereal user
            pass: process.env.MDP_GMAIL, // generated ethereal password
          },
        });

        // send mail with defined transport object
        let info = await transporter.sendMail({
          from: process.env.MAIL_GMAIL, // sender address
          to: user.email, // list of receivers
          subject: "Lien reset password", // Subject line
          text: "Hello world?", // plain text body
          html: html, // html body
        });
        return { msg: `Message sent: %s ${info.messageId}` }
      }
      return 'Wrong Code.'
    } catch (error) {
      throw new InternalError(error.errors)
    }
  }
  async unbookmark({ restaurantId }, user) {
    try {
      const checkIfExist = (element) => element._id == restaurantId;
      if (user.favoriteRestaurants.some(checkIfExist) === false)
        throw new NotFoundError('restaurant does not exist in favourite')
      await UserModel.findOneAndUpdate({ _id: user._id }, {
        $pull: { favoriteRestaurants: restaurantId },
      }, { multi: false })
      return 'restaurant removed from wishlist successfully!'
    } catch (error) {
      throw error
    }
  }
  async bookmark({ restaurantId }, user) {
    try {
      await UserModel.findOneAndUpdate({ _id: user._id }, {
        $addToSet: { favoriteRestaurants: restaurantId }
      })
      return 'restaurant added to wishlist successfully!'
    } catch (error) {
      throw error
    }
  }
  async unbookmarkAllRestaurants(user) {
    try {
      await UserModel.findOneAndUpdate({ _id: user._id }, {
        $set: { favoriteRestaurants: [] },
      })
      return 'All Restaaurants removed from wishlist successfully!'
    } catch (error) {
      throw error
    }
  }
  async usersList() {
    try {
      const users = await UserModel.find({}).lean()
      return users
    } catch (error) {
      throw error
    }
  }
}
module.exports = UserService
